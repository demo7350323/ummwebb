
function logout(){

    let ROLE = localStorage.getItem("ROLE");
    let url;
    if(ROLE.includes("Wrizto")){
        url = "./wLogin";
        $(".logout").attr('href',url);
    }else{
        url = "./cLogin";
        $(".logout").attr('href',url);
    }
    localStorage.removeItem("JWT_TOKEN");
	localStorage.removeItem("ROLE");
	localStorage.removeItem("CLIENT_ID");
    localStorage.removeItem("ORG_NAME");
	localStorage.removeItem("FULL_NAME");
    window.location.href = url;
}