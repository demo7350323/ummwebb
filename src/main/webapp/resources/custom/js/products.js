var	JWT_TOKEN = "", ROLE="", CLIENT_ID="";
var FULL_NAME 	= "";
var ORG_NAME 	= "";

$(document).ready(function(){ 

	JWT_TOKEN = localStorage.getItem("JWT_TOKEN");
	ROLE = localStorage.getItem("ROLE");
	CLIENT_ID = localStorage.getItem("CLIENT_ID");
	ORG_NAME = localStorage.getItem("ORG_NAME");
	FULL_NAME = localStorage.getItem("FULL_NAME");
	
	if(ROLE !== "Wrizto Super Admin" && ROLE !== "Wrizto Admin" && ROLE !== "Client Admin" || JWT_TOKEN.length === 0){
		var url = "/cLogin";
		window.location.href=url;
		return;
	}else if(ROLE === "Wrizto Super Admin" || ROLE === "Wrizto Admin"){
		loadProductTable();
		//API CALL to load Client dropdown
		getClientListForProduct();
		$(".hide-admin-unauth").css("display","none");
	}else if(ROLE === "Client Admin"){
		loadProductTable();
		$(".hide-unauth").css("display","none");
	}
	let portalDetails = "<h5>"+FULL_NAME+"</h5>";
	// portalDetails += "<p>"+ORG_NAME+"</p>";
	portalDetails += "<p style='margin: 0px;font-size: 12px;font-weight: 600;color: gold;'>"+ROLE+"</p>";

	$(".portal-user-name").html(portalDetails);
 }) 
 
 /* Load Product Table data when page loaded */
function loadProductTable(){
	console.log("loadProductTable->",ROLE, CLIENT_ID);
	$('#productTable').hide();
	$("#product-table-error").hide();
	$("#product-table-spinner").show();
	$('#product-table-spinner').css({display: 'flex',justifyContent: 'center',alignItems: 'center', height:'200px'});

	let url = CONF_SERVER_PATH + "/getProductTable?clientIds";

	if(ROLE.includes("Wrizto")){
		url += "&size="+CONF_MAX_LIMIT;
	}
	else if(ROLE.includes("Client")){
		url += "=" + CLIENT_ID + "&size="+CONF_MAX_LIMIT;
	}

	//API CALL
	$.ajax({
		type:'GET',
		url: url,
		headers: { "Authorization": 'Bearer ' + JWT_TOKEN},
        success: function(result) {
			$("#product-table-spinner").hide();
			
        	console.log('success: ',result);
        	if(result.error == true){
				$("#product-table-error").show();
				$('#product-table-error').css({display: 'flex',justifyContent: 'center',alignItems: 'center', height:'200px', color:'red'});
				$("#product-table-error").text(result.message);
			} else {
				if(!result.hasOwnProperty('content') || !result.content.hasOwnProperty('products')){	
					alert("Products field not exist");
				}
        	let {content, message, error} = result;
        	
        	//console.log("window.JWT_TOKEN-->", window.JWT_TOKEN);
        	$('#productTable').show();
        	
			$('#productTable').DataTable( {
			  	data: content.products.productList,
				destroy:true,  	
		        columns: [
			            {
			                data: "productId.productName"
			            },
			            {
			            	data: 'serialNo'
			            },
			            {
							data: null,
							render: function(data, type, row, meta) {								   
								if(row.hasOwnProperty('clientId') && row.clientId != null){
									//console.log("row.clientId: ",row.clientId);
									return row.clientId.clientName;
								} else{
									return '-';
								}			                    	                        
			                }				                
			            },
			            {
			            	data: null,
			                render: function(data, type, row, meta) {	
							//console.log('row: ', row);                    
			                    return row.productStatus === true ? 'Active' : 'Inactive';	                        
			                }	                
			            },
			            {
			            	data: null,
			                render: function(data, type, row, meta) {	                    
			                    return row.licenseStatus.licenseStatus;	                        
			                }
			            },
			            {
			            	data: null,
			                render: function(data, type, row, meta) {	                    
			                    return "Coming Soon!";	                        
			                }
			            },
			            {
			                data: 'null',
			                render: function(data, type, row, meta) {
			                    return "Coming Soon!";
			                }
			            },
			            {
							data: 'null',
							render: function(data, type, row, meta){
								//console.log("Data verification____>", row);
								let menuItem = '<div class="btn-group">' +
							    '<button type="button" class="btn btn-outline-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Menu</button>' +							 
							    '<div class="dropdown-menu dropdown-menu-right">'+
							    '<button class=\'dropdown-item\' type=\'button\' onClick=\'viewProductDetails('+ JSON.stringify(row) + ');\'>View Details</button>' +
							    '<button class=\'dropdown-item\' type=\'button\' onClick=\'loadAddSubscriberModal('+ JSON.stringify(row) + ');\'>Associate User</button>';
							    //'<button class=\'dropdown-item\' type=\'button\' onClick=\'activateLicense('+ JSON.stringify(row) + ');\'>Activate License</button>';							    
							    
							    if(row.licenseStatus.licenseStatus == 'NEW' || row.licenseStatus.licenseStatus == 'NOT_AT_ACTIVATED' && (ROLE === 'Wrizto Super Admin' || ROLE === 'Wrizto Admin' || ROLE === 'Client Admin')){
									menuItem += '<button class=\'dropdown-item\' type=\'button\' onClick=\'activateProductLicense('+ JSON.stringify(row) + ');\'>Assign License</button>';
									
									if(ROLE === 'Wrizto Super Admin' || ROLE === 'Wrizto Admin'){
										menuItem += '<button class=\'dropdown-item\' type=\'button\' onClick=\'linkClient('+ JSON.stringify(row) + ');\'>Assign Client</button>';										
									}
								} else{
									if(ROLE === 'Wrizto Super Admin' || ROLE === 'Wrizto Admin'){
										menuItem += '<button class=\'dropdown-item\' type=\'button\' onClick=\'changeClient('+ JSON.stringify(row) + ');\'>Change Client</button>';
									}
									menuItem += '<button class=\'dropdown-item\' type=\'button\' onClick=\'loadRemoveSubscriberModal('+ JSON.stringify(row) + ');\'>Disassociate User</button>';
								}							     
								menuItem += '</div></div>';		  
							  	return menuItem;
							}
						}
			        ]
		    } );
			}       	
    	},
    	error : function(xhr, status, error) {
			//console.log('loginByUserName->error: ', xhr, ", status: ",status, ', error:' ,xhr.responseJSON);
			$("#product-table-spinner").hide();
			$("#product-table-error").show();
			$('#product-table-error').css({display: 'flex',justifyContent: 'center',alignItems: 'center', height:'200px', color:'red'});
			if(xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')){					
				$("#product-table-error").text(xhr.responseJSON.message);
			}else {
				$("#product-table-error").text("No Data Found!");
			}
								
		}
    });
}

/* CALL API activate license of a product */
function activateProductLicense(product){
	$('#activate-client-license-modal').modal('show');

	let serialNo = product.serialNo;
	let clientId = product.clientId.id;


	let jsonData;
	
	if(serialNo == '' && clientId == ''){
		alert("Serial Number not found");
		return;
	}else{
		jsonData = JSON.stringify({
			"serialNo":serialNo,"clientId":clientId
		});
	}
	//API CALL to Register a User to a product

	$("#activateLicenseButton").unbind().click(function(){
		$.ajax({
			type:'POST',
			data:jsonData,
			contentType: "application/json",
			url: CONF_SERVER_PATH + "/activateLicense",
			headers: { "Authorization": 'Bearer ' + JWT_TOKEN},
			success: function(result) {
				
				console.log('success: ',result);
				if(result.error == true){
					//ToDo: Remove alert message
					alert(result.message);
				} else {
					
				let {content, message, error} = result;
				if(error === false){
					alert("Product Activated Successfully");
					loadProductTable();
				}		
				}       	
			},
			error : function(xhr, status, error) {
				// console.log('activateLicense->error: ', xhr, ", status: ",status, ', error:' ,xhr.responseJSON);
				alert(xhr.responseJSON.message);
				if(xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')){	
					//$("#errorMessage").text(xhr.responseJSON.message);
				}else {
					//$("#errorMessage").text("Server error");
				}
				
				//$('#errorMessage').fadeIn("slow");		
			}
		});
	});
	
	
}

var clientList = [];
function getClientListForProduct() {
	if (ROLE === "Wrizto Super Admin" || ROLE === "Wrizto Admin") {
		$.ajax({
			type: 'GET',
			url: CONF_SERVER_PATH + "/getClientList",
			headers: { "Authorization": 'Bearer ' + JWT_TOKEN },
			success: function (result) {
				if (!result.hasOwnProperty('content')) {
					alert("Token not exist");
				}
				let { content, message, error } = result;

				if(error === false){
					clientList = content.clientList;
				}

			},
			error: function (xhr, status, error) {
				console.log('error: ', error);
			}
		});
	}
}

/* CALL API activate license of a product */
function linkClient(product){

	if (ROLE === "Wrizto Super Admin" || ROLE === "Wrizto Admin") {
		$('#link-client-modal').modal('show');


		let clientId;
		let serialNo = product.serialNo;

		if(product.productStatus === true){

			if(serialNo === ""){
				alert("Serial number not found");
				return;
			}

			$("#linkClientSerial").html("<p>"+serialNo+"</p>");
			$("#linkClientProduct").html("<p>"+product.productId.productName+"</p>");

			let clientDropdownList ="<option selected>Select Client</option>";
				
			for (let client of clientList){
				clientDropdownList += "<option value="+ client.id +" >"+client.name+"</option>"		    
			}
					
			$("#linkClientSelector").html(clientDropdownList);

			$("#linkClientSelector").change(function(){
				clientId = $("#linkClientSelector").val();
			});

			$("#linkClientButton").unbind().click(function(){


				if(!isNaN(clientId)){

					let jsonData = JSON.stringify({
						"serialNo":serialNo,"clientId":Number(clientId)
					})
					
					//API CALL to link Client to a product
					
					$.ajax({
						type:'POST',
						url: CONF_SERVER_PATH + "/linkProductToClient",
						data:jsonData,
						contentType: "application/json",
						headers: { "Authorization": 'Bearer ' + JWT_TOKEN},
						success: function(result) {
							
							console.log('success: ',result);
							if(result.error == true){
								//ToDo: Remove alert message
								alert(result.message);
							} else {
								
							let {content, message, error} = result;
		
							if(error === false){
								alert("Product with serial: "+ serialNo + " Linked to Client "+ $("#linkClientSelector").find('option:selected').text());
								$('#link-client-modal').modal('hide');
								loadProductTable();
							}
							
							//console.log("window.JWT_TOKEN-->", window.JWT_TOKEN);
							
							
							}       	
						},
						error : function(xhr, status, error) {
							//console.log('activateLicense->error: ', xhr, ", status: ",status, ', error:' ,xhr.responseJSON);
		
							alert(xhr.responseJSON.message);
							
							if(xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')){	
								//$("#errorMessage").text(xhr.responseJSON.message);
							}else {
								//$("#errorMessage").text("Server error");
							}
							
							//$('#errorMessage').fadeIn("slow");		
						}
					});
				}else{
					alert("Please choose a client");
				}
			});


		}else{
			alert("This product status is Inactive");
		}
	}else{
		alert("Feature only available to Wrizto Admins");
	}

}

/* CALL API activate license of a product */
function changeClient(product){
	if (ROLE === "Wrizto Super Admin" || ROLE === "Wrizto Admin") {
		$('#update-client-modal').modal('show');

		let clientId;
		let serialNo = product.serialNo;

		if(product.productStatus === true){

			let productId = product.id;

			if(serialNo === ""){
				alert("Serial number not found");
				return;
			}

			$("#changeClientSerial").html("<p>"+serialNo+"</p>");
			$("#changeClientProduct").html("<p>"+product.productId.productName+"</p>");

			let clientDropdownList ="<option selected value="+ product.clientId.id + ">"+ product.clientId.clientName +"</option>";
				
			for (let client of clientList){
				if(client.id === product.clientId.id){
					continue;
				}
				clientDropdownList += "<option value="+ client.id +">"+client.name+"</option>"		    
			}
					
			$("#changeClientSelector").html(clientDropdownList);

			$("#changeClientSelector").change(function(){
				clientId = $("#changeClientSelector").val();
			});

			$("#changeClientButton").unbind().click(function(){


				if(!isNaN(clientId)){

					let jsonData = JSON.stringify({
						"serialNo":serialNo,"clientId":Number(clientId),"productId":Number(productId)
					})
					
					//API CALL to change Client to a product
					
					$.ajax({
						type:'POST',
						url: CONF_SERVER_PATH + "/modifyClient",
						data:jsonData,
						contentType: "application/json",
						headers: { "Authorization": 'Bearer ' + JWT_TOKEN},
						success: function(result) {
							
							console.log('success: ',result);
							if(result.error == true){
								//ToDo: Remove alert message
								alert(result.message);
							} else {
								
							let {content, message, error} = result;
		
							if(error === false){
								alert("Product with serial: "+ serialNo + " Linked to Client "+ $("#updateClientSelector").find('option:selected').text());
								$('#update-client-modal').modal('hide');
								loadProductTable();
							}
							
							//console.log("window.JWT_TOKEN-->", window.JWT_TOKEN);
							
							
							}       	
						},
						error : function(xhr, status, error) {
							//console.log('activateLicense->error: ', xhr, ", status: ",status, ', error:' ,xhr.responseJSON);
		
							alert(xhr.responseJSON.message);
							
							if(xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')){	
								//$("#errorMessage").text(xhr.responseJSON.message);
							}else {
								//$("#errorMessage").text("Server error");
							}
							
							//$('#errorMessage').fadeIn("slow");		
						}
					});
				}else{
					alert("Please choose a client");
				}
			});


		}else{
			alert("This product status is Inactive");
		}
	}else{
		alert("Feature only available to Wrizto Admins");
	}
}

/* CALL API to add new subscriber to a product */
function loadAddSubscriberModal(product){
	console.log("loadAddSubscriberModal->start: ",JWT_TOKEN);
	$('#loadAddSubscriber-modal').modal('show');

	let clientId = product.clientId.id;
	let productId = product.id;
	let userId;

	let userList;

		$('#addSubRoleSelector').html("<option selected>Select Role</option>");
		$('#addSubUserSelector').html("<option selected>Choose Role above</option>");

		$.ajax({
			type:'GET',
			url: CONF_SERVER_PATH + "/getRoles",
			headers: { "Authorization": 'Bearer ' + JWT_TOKEN },
			success: function(result) {
				console.log(result);
				if(!result.hasOwnProperty('content')){	
					alert("Token not exist");
				}
				let {content, message} = result;
				let {roles} = content;
				
				let roleDropdownList ="<option selected>Select Role</option>";
				for (let role of roles){
					if(role.name.includes("Doctor") || role.name.includes("Nurse") || role.name.includes("Consumer")){
						roleDropdownList += "<option value="+ role.id +" >"+role.name+"</option>";
					}
				}
				$("#addSubRoleSelector").html(roleDropdownList);
				
				// else if (mode === 'update'){
				// 	let roleDropdownList ="<option selected value="+select.id +">"+select.name+"</option>";
				// 	for (let role of roles){
				// 		if(role.id === select.id){
				// 			continue;
				// 		}
				// 		roleDropdownList += "<option value="+ role.id +" >"+role.name+"</option>"		    
				// 	}
				// 	$("#roleSelector1").html(roleDropdownList);
				// }
	
			},
			error : function(xhr, status, error) {
				console.log(error);
			}
		});

		// console.log(product);


		$.ajax({
			type:'GET',
			url: CONF_SERVER_PATH + "/getSubscriberList?clientId="+clientId+"&productId="+productId+"&status=unsubscribed",
			headers: { "Authorization": 'Bearer ' + JWT_TOKEN},
			success: function(result) {
				
				console.log('success: ',result);
				if(result.error == true){
					console.log("Inside if");
					
				}else{
					console.log("Inside else");
				
				let {content, message, error} = result;
					userList = result;
				}
			},
			error : function(xhr, status, error) {
				//console.log('viewProductDetails->error: ', xhr, ", status: ",status, ', error:' ,xhr.responseJSON);
	
				if(error === true){
					alert(xhr.responseJSON.message);
				}
				
				if(xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')){	
					//$("#errorMessage").text(xhr.responseJSON.message);
				}else {
					//$("#errorMessage").text("Server error");
				}
				
				//$('#errorMessage').fadeIn("slow");		
			}
		});
		
		$("#addSubRoleSelector").change(function(){
				if(userList!==undefined){
					if(userList.content.doctor != [] && $("#addSubRoleSelector").find('option:selected').text() === "Doctor"){
						let doctorList = "<option selected>Select Doctor</option>";
						for(tempDoctor of userList.content.doctor){
							doctorList += "<option value="+ tempDoctor.id +" >"+tempDoctor.name+"</option>"
						}
						$('#addSubUserSelector').html(doctorList);
					}
					else if(userList.content.nurse != [] && $("#addSubRoleSelector").find('option:selected').text() === "Nurse"){
						
						let nurseList = "<option selected>Select Nurse</option>";
						for(tempNurse of userList.content.nurse){
							nurseList += "<option value="+ tempNurse.id +" >"+tempNurse.name+"</option>";
						}
						$('#addSubUserSelector').html(nurseList);
					}
					else if(userList.content.consumer != [] && $("#addSubRoleSelector").find('option:selected').text() === "Consumer"){
						console.log("Consumer ",userList.content.consumer);
						let consumerList = "<option selected>Select Consumer</option>";
						for(tempConsumer of userList.content.consumer){
							consumerList += "<option value="+ tempConsumer.id +" >"+tempConsumer.name+"</option>";
						}
						$('#addSubUserSelector').html(consumerList);
					}else{
						$('#addSubUserSelector').html("No User found");
					}
				}else{
					$('#addSubUserSelector').html("No User found");
				}		

		});


		$("#addSubButton").unbind().click(function(){

			userId = $('#addSubUserSelector').val();

			if(userId.includes("Select")){
				alert("Choose User");
				return;
			}
			
			let jsonData = JSON.stringify({
				"clientId":clientId,"userId":Number(userId),"productId":Number(productId)});

			//API CALL to Register a User to a product
			$.ajax({
				type:'POST',
				data:jsonData,
				contentType: "application/json",
				url: CONF_SERVER_PATH + "/subscribeToProduct",
				headers: { "Authorization": 'Bearer ' + JWT_TOKEN},
				success: function(result) {
					
					console.log('success: ',result);
					if(result.error == true){
						//ToDo: Remove alert message
						alert(result.message);
					} else {
						if(!result.hasOwnProperty('content') || !result.content.hasOwnProperty('products')){
							//ToDo: Remove alert message
							// alert("Products field not exist");
						}
					let {content, message, error} = result;
					if(error === false){
						alert("Product Subscribed successfully");
					}
					
					
					//console.log("window.JWT_TOKEN-->", window.JWT_TOKEN);
					}       	
				},
				error : function(xhr, status, error) {
					//console.log('loadAddSubscriberModal->error: ', xhr, ", status: ",status, ', error:' ,xhr.responseJSON);

					alert(xhr.responseJSON.message);
					
					if(xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')){	
						//$("#errorMessage").text(xhr.responseJSON.message);
					}else {
						//$("#errorMessage").text("Server error");
					}
					
					//$('#errorMessage').fadeIn("slow");		
				}
			});

		});

		
		
}

/* CALL API to add new subscriber to a product */
function loadRemoveSubscriberModal(product){
	$('#loadRemoveSubscriber-modal').modal('show');

	let roleId;
	let clientId = product.clientId.id;
	let productId = product.id;
	let userId;

	let userList;

	$('#removeSubRoleSelector').html("<option selected>Select Role</option>");
	$('#removeSubUserSelector').html("<option selected>Choose Role above</option>");

		$.ajax({
			type:'GET',
			url: CONF_SERVER_PATH + "/getRoles",
			headers: { "Authorization": 'Bearer ' + JWT_TOKEN },
			success: function(result) {
				console.log(result);
				if(!result.hasOwnProperty('content')){	
					alert("Token not exist");
				}
				let {content, message} = result;
				let {roles} = content;
				
				let roleDropdownList ="<option selected>Select Role</option>";
				for (let role of roles){
					if(role.name.includes("Doctor") || role.name.includes("Nurse") || role.name.includes("Consumer")){
						roleDropdownList += "<option value="+ role.id +" >"+role.name+"</option>";
					}
				}
				$("#removeSubRoleSelector").html(roleDropdownList);
	
			},
			error : function(xhr, status, error) {
				console.log(error);
			}
		});
		roleId = $("#removeSubRoleSelector").val();



		$.ajax({
			type:'GET',
			url: CONF_SERVER_PATH + "/getSubscriberList?clientId="+clientId+"&productId="+productId+"&status=subscribed",
			headers: { "Authorization": 'Bearer ' + JWT_TOKEN},
			success: function(result) {
				
				console.log('success: ',result);
				if(result.error == true){
					console.log("Inside if");
					
				}else{
					console.log("Inside else");
				
				let {content, message, error} = result;
					userList = result;
				}
			},
			error : function(xhr, status, error) {
				//console.log('viewProductDetails->error: ', xhr, ", status: ",status, ', error:' ,xhr.responseJSON);
	
				if(error === true){
					alert(xhr.responseJSON);
				}
				
				if(xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')){	
					//$("#errorMessage").text(xhr.responseJSON.message);
				}else {
					//$("#errorMessage").text("Server error");
				}
				
				//$('#errorMessage').fadeIn("slow");		
			}
		});

		$("#removeSubRoleSelector").change(function(){
			if(userList!==undefined){
				if(userList.content.doctor != [] && $("#removeSubRoleSelector").find('option:selected').text() === "Doctor"){
					let doctorList = "<option selected>Select Doctor</option>";
					for(tempDoctor of userList.content.doctor){
						doctorList += "<option value="+ tempDoctor.id +" >"+tempDoctor.name+"</option>";
					}
					$('#removeSubUserSelector').html(doctorList);
				}
				else if(userList.content.nurse != [] && $("#removeSubRoleSelector").find('option:selected').text() === "Nurse"){
					
					let nurseList = "<option selected>Select Nurse</option>";
					for(tempNurse of userList.content.nurse){
						nurseList += "<option value="+ tempNurse.id +" >"+tempNurse.name+"</option>";
					}
					$('#removeSubUserSelector').html(nurseList);
				}
				else if(userList.content.consumer != [] && $("#removeSubRoleSelector").find('option:selected').text() === "Consumer"){
					
					let consumerList = "<option selected>Select Consumer</option>";
					for(tempConsumer of userList.content.consumer){
						consumerList += "<option value="+ tempConsumer.id +" >"+tempConsumer.name+"</option>";
					}
					$('#removeSubUserSelector').html(consumerList);
				}else{
					$('#removeSubUserSelector').html("No User found");
				}
			}else{
				$('#removeSubUserSelector').html("No User found");
			}
			
	
	});

	$("#removeSubButton").unbind().click(function(){

		userId = $('#removeSubUserSelector').val();

		if(userId.includes("Select")){
			alert("Choose User");
			return;
		}
		
		let jsonData = JSON.stringify({
			"clientId":clientId,"userId":Number(userId),"productId":Number(productId)});

		//API CALL to Unsubscribe a User to a product
		$.ajax({
			type:'POST',
			data:jsonData,
			contentType: "application/json",
			url: CONF_SERVER_PATH + "/unsubscribeFromProduct",
			headers: { "Authorization": 'Bearer ' + JWT_TOKEN},
			success: function(result) {
				
				console.log('success: ',result);
				if(result.error == true){
					//ToDo: Remove alert message
					alert(result.message);
				} else {
				let {content, message, error} = result;
				if(error === false){
					alert("Product Unsubscribed successfully");
				}
				
				
				//console.log("window.JWT_TOKEN-->", window.JWT_TOKEN);
				}       	
			},
			error : function(xhr, status, error) {
				//console.log('loadAddSubscriberModal->error: ', xhr, ", status: ",status, ', error:' ,xhr.responseJSON);

				alert(xhr.responseJSON.message);
				
				if(xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')){	
					//$("#errorMessage").text(xhr.responseJSON.message);
				}else {
					//$("#errorMessage").text("Server error");
				}
				
				//$('#errorMessage').fadeIn("slow");		
			}
		});

	});
		
}


/* Load Product deatils modal when clicked on action menu (view details) */
function viewProductDetails(productObj){
	console.log("viewProductDetails->start: ",productObj);
	$('#productDetails-modal').modal('show');
	//API CALL to fetch Registerd Users for a product
	
	let clientId;

	if(ROLE === "Client Admin"){
		clientId = CLIENT_ID;
	}else{
		clientId = productObj.clientId.id;
	}

	$('#productCardSerial').html(productObj.serialNo);
	$('#productCardName').html(productObj.productId.productName);

	let url = CONF_SERVER_PATH + "/getSubscriberList?clientId="+clientId;
	if(productObj.id != ''){
		url += "&productId=" + productObj.id + "&status=subscribed";
	}

	$('#collapseOne').html('');
	$('#collapseTwo').html('');
	$('#collapseThree').html('');
	$('#collapseOne').removeClass('show');
	$('#collapseTwo').removeClass('show');
	$('#collapseThree').removeClass('show');


	$.ajax({
		type:'GET',
		url: url,
		headers: { "Authorization": 'Bearer ' + JWT_TOKEN},
        success: function(result) {
			
        	console.log('success: ',result);
        	if(result.error == true){
				//ToDo: Remove alert message
				$('#doctorTitle').html("Doctors <span class='float-right badge bg-info'>0</span>");
				$('#nurseTitle').html("Nurses <span class='float-right badge bg-danger'>0</span>");
				$('#consumerTitle').html("Consumers <span class='float-right badge bg-success'>0</span>");
				$('#collapseOne').removeClass('show');
				$('#collapseTwo').removeClass('show');
				$('#collapseThree').removeClass('show');
				// $('#collapseOne').html('No Data');
				// $('#collapseTwo').html('No Data');
				// $('#collapseThree').html('No Data');
			} else {
			
        	let {content, message, error} = result;

			$('#doctorTitle').html("Doctors <span class='float-right badge bg-info'>0</span>");
			$('#nurseTitle').html("Nurses <span class='float-right badge bg-danger'>0</span>");
			$('#consumerTitle').html("Consumers <span class='float-right badge bg-success'>0</span>");
        	
        	// console.log(content);
			if(content.doctor != ''){
				console.log(content.doctor);
				$('#doctorTitle').html("Doctors <span class='float-right badge bg-info'>"+ content.doctor.length +"</span>");
				let doctorList = '';
				for(tempDoctor of content.doctor){
					doctorList += "<p>"+tempDoctor.name+"</p>";
				}
				$('#collapseOne').html(doctorList);
			}else{
				$('#collapseOne').removeClass('show');
			}
			if(content.nurse != ''){
				$('#nurseTitle').html("Nurses <span class='float-right badge bg-danger'>"+ content.nurse.length +"</span>");
				
				let nurseList = '';
				for(tempNurse of content.nurse){
					nurseList += "<p>"+tempNurse.name+"</p>";
				}
				$('#collapseTwo').html(nurseList);
			}else{
				
				$('#collapseTwo').removeClass('show');
	
			}
			if(content.consumer != ''){
				$('#consumerTitle').html("Consumers <span class='float-right badge bg-success'>"+ content.consumer.length +"</span>");
				
				let consumerList = '';
				for(tempConsumer of content.consumer){
					consumerList += "<p>"+tempConsumer.name+"</p>";
				}
				$('#collapseThree').html(consumerList);
			}else{
				
				$('#collapseThree').removeClass('show');
			}
        	
			
			}       	
    	},
    	error : function(xhr, status, error) {
			//console.log('viewProductDetails->error: ', xhr, ", status: ",status, ', error:' ,xhr.responseJSON);

			if(error === true){
				$('#doctorTitle').html("Doctors <span class='float-right badge bg-info'>0</span>");
				$('#nurseTitle').html("Nurses <span class='float-right badge bg-danger'>0</span>");
				$('#consumerTitle').html("Consumers <span class='float-right badge bg-success'>0</span>");
				$('#collapseOne').removeClass('show');
				$('#collapseTwo').removeClass('show');
				$('#collapseThree').removeClass('show');
			}
			
			if(xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')){	
				//$("#errorMessage").text(xhr.responseJSON.message);
			}else {
				//$("#errorMessage").text("Server error");
			}
			
			//$('#errorMessage').fadeIn("slow");		
		}
    });
}

