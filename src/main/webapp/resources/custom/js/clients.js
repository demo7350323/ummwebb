var	JWT_TOKEN 	= "";
var ROLE 		= "";
var FULL_NAME 	= "";
var ORG_NAME 	= "";

$(document).ready(function(){

	JWT_TOKEN = localStorage.getItem("JWT_TOKEN");
	ROLE = localStorage.getItem("ROLE");
	ORG_NAME = localStorage.getItem("ORG_NAME");
	FULL_NAME = localStorage.getItem("FULL_NAME");


	if(ROLE !== "Wrizto Super Admin" && ROLE !== "Wrizto Admin" || JWT_TOKEN.length === 0){
		var url = "./wLogin";
		window.location.href=url;
		return;
	}
	loadClientTable();

	$(".hide-admin-unauth").css("display","none");

	let portalDetails = "<h5>"+FULL_NAME+"</h5>";
	// portalDetails += "<p>"+ORG_NAME+"</p>";
	portalDetails += "<p style='margin: 0px;font-size: 12px;font-weight: 600;color: gold;'>"+ROLE+"</p>";

	$(".portal-user-name").html(portalDetails);
 })

function loadClientTable(){
	
	$('#clientTable').hide();
	$("#client-table-error").hide();
	$("#client-table-spinner").show();
	$('#client-table-spinner').css({display: 'flex',justifyContent: 'center',alignItems: 'center', height:'200px'});
	//API CALL
	
	$.ajax({
		type:'GET',
		url: CONF_SERVER_PATH + "/getClientTable?clientIds&size="+CONF_MAX_LIMIT,
		headers: { "Authorization": 'Bearer ' + JWT_TOKEN },
        success: function(result) {
	
			$("#client-table-spinner").hide();
			
        	console.log('success: ',result);
        	if(result.error == true){
				$("#client-table-error").show();
				$('#client-table-error').css({display: 'flex',justifyContent: 'center',alignItems: 'center', height:'200px', color:'red'});
				$("#client-table-error").text(result.message);
			} else {
				if(!result.hasOwnProperty('content') || !result.content.hasOwnProperty('clients')){	
				alert("Client field not exist");
				}
	        	let {content, message, error} = result;
	        	
	        	//console.log("JWT_TOKEN-->", JWT_TOKEN);
	        	$('#clientTable').show();
	        	
				$('#clientTable').DataTable({
				  	data: content.clients.clientList,	  	
			        columns: [
			            {
			                data: 'clientName'
			            },
			            {
			                data: 'clientId'
			            },
			            {
			                data: null,
			                render: function(data, type, row, meta) {
			                	let address = "";
			                	if(row.addressLine1) address= row.addressLine1 + ", ";
			                	if(row.addressLine2) address += row.addressLine2 + ", ";
			                	if(row.city) address += row.city + ", ";
			                	if(row.state) address += row.state + ", ";
			                	if(row.pinCode) address += row.pinCode;
			                    return address;
			                }
			            },
			            {
			            	data: null,
							render: function(data, type, row, meta) {
			                    return "Yet to be implemented";
			                }                
			            },
			            {
			            	data: null,
			                render: function(data, type, row, meta) {	                    
			                    return row.status === true ? 'Active' : 'Inactive;'	                        
			                }	                
			            }
			        ]
			    });
			}      	
    	},
    	error : function(xhr, status, error) {
			//console.log('loginByUserName->error: ', xhr, ", status: ",status, ', error:' ,xhr.responseJSON);
			$("#client-table-spinner").hide();
			$("#client-table-error").show();
			$('#client-table-error').css({display: 'flex',justifyContent: 'center',alignItems: 'center', height:'200px', color:'red'});
			if(xhr.hasOwnProperty('responseJSON') && xhr.responseJSON.hasOwnProperty('message')){					
				$("#client-table-error").text(xhr.responseJSON.message);
			}else {
				$("#client-table-error").text("No Data Found!");
			}
	
		}
    });
}

function addClient(){


		console.log("Inside inviteclient");

		let jsonData;
	
			if( $("#inviteClientName").val() == ""){
				console.log("Empty name input field found!");
				$("#errorClientMessage1").text("Empty name input field found!");
				$('#errorClientMessage1').fadeIn("slow");
				return;
			}
			else if( $("#inviteClientPhone").val() == ""){
				console.log("Empty phone number field found!");
				$("#errorClientMessage1").text("Empty phone number field found!");
				$('#errorClientMessage1').fadeIn("slow");
				return;
			}
			else if( $("#inviteClientEmail").val() == ""){
				console.log("Empty email field found!");
				$("#errorClientMessage1").text("Empty email field found!");
				$('#errorClientMessage1').fadeIn("slow");
				return;
			}
			else if( $("#inviteClientAddLine1").val() == ""){
				console.log("Empty address line 1 field found!");
				$("#errorClientMessage1").text("Empty address line 1 field found!");
				$('#errorClientMessage1').fadeIn("slow");
				return;
			}
			else if( $("#inviteClientCity").val() == ""){
				console.log("Empty city field found!");
				$("#errorClientMessage1").text("Empty city field found!");
				$('#errorClientMessage1').fadeIn("slow");
				return;
			}
			else if( $("#inviteClientState").val() == ""){
				console.log("Empty state field found!");
				$("#errorClientMessage1").text("Empty state field found!");
				$('#errorClientMessage1').fadeIn("slow");
				return;
			}
			else if( $("#inviteClientPinCode").val() == ""){
				console.log("Empty pin code field found!");
				$("#errorClientMessage1").text("Empty pin code field found!");
				$('#errorClientMessage1').fadeIn("slow");
				return;
			}
			
			let formData = {
				"ownerFirstname":$("#inviteClientName").val(),
				"contactNo":$("#inviteClientPhone").val(),
				"ownerEmail":$("#inviteClientEmail").val(),
				"address_line_1":$("#inviteClientAddLine1").val(),
				"address_city":$("#inviteClientCity").val(),
				"address_state":$("#inviteClientState").val(),
				"pincode":$("#inviteClientPinCode").val()
			}


			if($("#inviteClientAddLine2").val() !== ""){
				formData = Object.assign(formData,{"address_line_2":$("#inviteClientAddLine2").val()});
			}

			jsonData = JSON.stringify(formData);
			
			
		
			//API CALL
			$.ajax({
				type:'POST',
				data:jsonData,
				contentType: "application/json",
				url: CONF_SERVER_PATH + "/addClient",
				headers: { "Authorization": 'Bearer ' + JWT_TOKEN },
				success: function(result) {
					console.log("inviteClient-->2: ",result);
					
					if(result.error===false){
						$("#inviteClientName").val('');
						$("#inviteClientPhone").val(''),
						$("#inviteClientEmail").val(''),
						$("#inviteClientAddLine1").val(''),
						$("#inviteClientCity").val(''),
						$("#inviteClientState").val(''),
						$("#inviteClientPinCode").val('')
						$("#inviteClientAddLine2").val('')
						alert("Client Added Successfully");
						$('#invite-client-modal').modal('hide');
						location.reload();
					}
		
				},
				error : function(xhr, status, error) {
					console.log("inviteClient-->3 : ",error);
					alert(xhr.jsonData.message);
				}
			});
		

}