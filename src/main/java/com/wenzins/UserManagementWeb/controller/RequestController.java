package com.wenzins.UserManagementWeb.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class RequestController {
	
	@RequestMapping(value= {"/","/cLogin"}, method = {RequestMethod.GET} )
	public String login(HttpServletRequest request,HttpServletResponse response){
		System.out.println("Inside /clientLogin");
		return "clientLogin";
	}
	@RequestMapping(value= {"/wLogin"}, method = {RequestMethod.GET} )
	public String wriztoLogin(HttpServletRequest request,HttpServletResponse response){
		System.out.println("Inside /wriztoLogin");
		return "wriztoLogin";
	}
	
	@RequestMapping(value= {"/dashboard"}, method = {RequestMethod.GET} )
	public String dashboard(HttpServletRequest request,HttpServletResponse response){
		System.out.println("Inside /dashboard");
		return "dashboard";
	}
	
	@RequestMapping(value= {"/clients"}, method = {RequestMethod.GET} )
	public String clients(HttpServletRequest request,HttpServletResponse response){
		System.out.println("Inside /clients");
		return "manageClients";
	}

	@RequestMapping(value= {"/products"}, method = {RequestMethod.GET} )
	public String manageProducts(HttpServletRequest request,HttpServletResponse response){
		System.out.println("Inside /products");
		return "manageProducts";
	}
	
	@RequestMapping(value= {"/users"}, method = {RequestMethod.GET} )
	public String manageUsers(HttpServletRequest request,HttpServletResponse response){
		System.out.println("Inside /users");
		return "manageUsers";
	}
	
	@RequestMapping(value= {"/logs"}, method = {RequestMethod.GET} )
	public String manageLogs(HttpServletRequest request,HttpServletResponse response){
		System.out.println("Inside /logs");
		return "manageLogs";
	}
	
	@RequestMapping(value= {"/tickets"}, method = {RequestMethod.GET} )
	public String manageTickets(HttpServletRequest request,HttpServletResponse response){
		System.out.println("Inside /tickets");
		return "validation";
	}
	
	
}
