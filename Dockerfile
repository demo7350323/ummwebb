
FROM openjdk:8-jdk-alpine
ARG WAR_FILE=target/ummWeb.war
WORKDIR /opt/git_projects/user-management-web
COPY ${WAR_FILE} ummWeb.war
EXPOSE 8085
ENTRYPOINT [ "java", "-jar", "ummWeb.war" ]

